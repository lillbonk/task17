const readline = require("readline");
const os = require("os");


const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const message = `
Choose and option:
1. Read my package.json
2: Get my OD info
3: Start a HTTP server
-----------------
Type a number: `;

rl.question(message, answer => {
 
  if (answer == 1) {
    console.log("Reading package.json file");
    require("./readline.js").content;
  } else if (answer == 2) {
      console.log("Gettign OS info...");
      console.log("SYSTEM MEMORY:",(os.totalmem() / 1024 / 1024 / 1024).toFixed(2) + " GB");
      console.log("FREE MEMORY:",(os.freemem() / 1024 /1024 / 1024).toFixed(2) + " GB");
      console.log("CPU CORES:", os.cpus().length);
      console.log("ARCH:",os.arch() );
      console.log("PLATFORM:", os.platform() );
      console.log("RELEASE:", os.release() );
      console.log("USER:", os.userInfo().username);   
         
      

  } else if (answer == 3) {
    
    console.log("Starting HTTP server...");
        require("./http.js");


  } else {
    console.log(
      answer + " is not a valid option, please choose a correct number");
  }

  rl.close();
});